//
//  AddDetailViewController.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/2/19.
//

import UIKit
import DropDown

protocol ChildViewControllerDelegate
{
    func childViewControllerResponse(detail: [Json4Swift_Base], plan: [LstPlanning])
}

class AddDetailViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var saveBtnTitle: UIButton!
    @IBOutlet weak var backShift: UIButton!
   
    @IBOutlet weak var addTtitle: UILabel!
    var yesNoArray = [[String: Any]]()
    @IBOutlet weak var onClickBackModel: UIButton!
    @IBOutlet weak var backViewModel: UIButton!
      @IBOutlet weak var backViewStatus: UIButton!
    @IBOutlet weak var shiftOutlet: UIButton!
    var delegate: ChildViewControllerDelegate?
    var otherSectionModels :[Json4Swift_Base]!
    var planningModel : [LstPlanning]!
    var isEditBool = "false"
     var checkEdit = "false"
    var detailID = "0"
    var sectionParam = [String:Any]()
    var currentTxt = UITextField()
    var currentTxtView = UITextView()
    var textFieldModelSelection = UITextField()
    var lstPlanningData = [[String:AnyObject]]()
    var requestPlanningData = [[String:AnyObject]]()

    var valueParam = [[String:AnyObject]]()
    var sectionPlanningData = [String: AnyObject]()
    var param = [String:Any]()
    var rowCount = [1]
    var cellIndex = Int()
    let backView = UIView()
    var shiftId = 1
    var modelId : Int?
    
    @IBOutlet weak var addbtnTag: UIButton!
    @IBOutlet var addRowView: UIView!
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var qtyTxt: UITextField!
    @IBOutlet weak var toChessisTxt: UITextField!
    @IBOutlet weak var fromChesisTxt: UITextField!
    @IBOutlet weak var modelTxt: UITextField!
    @IBOutlet weak var titleLbl: UILabel!
    var currentSender : AnyObject!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var txtProduction: UITextField!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtShift: UITextField!
   
    @IBOutlet weak var tapVew: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let dict1 = ["selection" : "Yes"]
        let dict2 = ["selection" : "No"]
       
        yesNoArray.append(dict1)
        yesNoArray.append(dict2)
        setBackViewValues()
        setUpInitialView()
        setEditData()
         
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Constant.editData = "AddScreen"
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = self // This is not required
        tapVew.addGestureRecognizer(tap)
        
    }
    
    @IBAction func backStatusBtn(_ sender: Any) {
        setYesNoDropDown()
    }
    
    @IBAction func shiftBtnAction(_ sender: AnyObject) {
        setButtonDropDown()
    }
    
    @IBAction func onClickModel(_ sender: Any) {
        dropDown()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        
        //let vc = ViewDetailsViewController()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickBackShift(_ sender: Any) {
         setButtonDropDown()
    }
    
    @IBAction func onClickCancel(_ sender: Any) {
        self.backView.removeFromSuperview()
    }
    
    @IBAction func onClickAddBtn(_ sender: Any) {
        
        if (fromChesisTxt.text?.isEmpty == true || toChessisTxt.text?.isEmpty == true || qtyTxt.text?.isEmpty == true){
            self.view.makeToast("Please fill the data first")
            return
        }
        if (backViewModel.titleLabel?.text == nil || backViewStatus.titleLabel?.text == nil){
            self.view.makeToast("Please fill the data first")
            return
        }
        var templstPlanningData = [String:AnyObject]()
        let shift = backViewModel.titleLabel?.text
//        let array = shift?.components(separatedBy: " ")
//        let id = array?[1] as! String
        //templstPlanningData["ModelValue"] = backViewModel.titleLabel?.text
        templstPlanningData["ModelId"] = self.modelId as AnyObject
        templstPlanningData["Model"] = shift as AnyObject
        templstPlanningData["FromChassis"] = fromChesisTxt.text as AnyObject
        templstPlanningData["ToChassis"] = toChessisTxt.text as AnyObject
        templstPlanningData["Quantity"] = qtyTxt.text as AnyObject
        templstPlanningData["Status"] = backViewStatus.titleLabel?.text as AnyObject
        
        if (addbtnTag.tag == 90){
            lstPlanningData.append(templstPlanningData)
            valueParam.append(templstPlanningData)
        }
        else {
            if (lstPlanningData.count > 0){
                lstPlanningData[self.cellIndex] = templstPlanningData
            }
            else {
                lstPlanningData.append(templstPlanningData)
            }
        }
        self.backView.removeFromSuperview()
       tableView.reloadData()
    }
    
    @IBAction func addBtnAction(_ sender: Any) {
        setBackViewValues()
        addbtnTag.tag = 90
        self.backView.isHidden = false
        backView.frame = CGRect (x: 0, y:0, width: view.frame.size.width , height: view.frame.size.height)
        backView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
       
       // addRowView.frame = CGRect(x: 10, y:80, width: view.frame.size.width - 20, height: view.frame.size.height - 150)
        addRowView.center = backView.center
        view.addSubview(backView)
        backView.addSubview(addRowView)
        view.bringSubviewToFront(addRowView)
        
    }
    
    @IBAction func onClickBtnActions(_ sender: AnyObject) {
        var lstPlanning = [[String:String]]()
        var tempSectionDict = [String:String]()
       if (sender.tag == 100){
        if (txtDate.text == ""){
            self.view.makeToast("Please fill the date first")
           return
        }
        if (lstPlanningData.count == 0){
            self.view.makeToast("At least one record for planning data is mandatory")
            return
        }
        else {
            for data in lstPlanningData{
                
                var  templstPlanningData = [String:AnyObject]()
                
                templstPlanningData["ModelId"] = data["ModelId"] as AnyObject
                templstPlanningData["FromChassis"] = data["FromChassis"] as AnyObject
                templstPlanningData["ToChassis"] = data["ToChassis"] as AnyObject
                templstPlanningData["Quantity"] = data["Quantity"] as AnyObject
                templstPlanningData["Status"] = data["Status"] as AnyObject
                requestPlanningData.append(templstPlanningData)
            }
        }
        if (shiftOutlet.tag == 3){
            if (shiftOutlet.titleLabel?.text == ""){
                self.view.makeToast("Please fill shift")
                 return
            }
        }
        
            if lstPlanningData.count <= 0{
                self.view.makeToast("At least one record for planning data is mandatory")
                return
            }
        let present = sectionParam["Present"] as? String
        let absent = sectionParam["Absent"] as? String
        let totalManpower = sectionParam["TotalManpower"] as? String


            if (present == nil || present == "")
            {
                 self.view.makeToast("Absenteeism data is mandatory")
                return
            }
            else if (absent == nil || absent == ""){
                self.view.makeToast("Absenteeism data is mandatory")
                return
        }
            else if (totalManpower == nil || totalManpower == ""){
                self.view.makeToast("Absenteeism data is mandatory")
                return
        }
            param["UserId"] = Constant.userData?.userId as AnyObject
            //tempSectionDict["UserId"] = Constant.userData?.userId
            param["Date"] = txtDate.text as AnyObject
            tempSectionDict["Date"] = txtDate.text
            let shift = shiftOutlet.titleLabel?.text
//        if (shiftOutlet.titleLabel?.text == "Shift A"){
//            param["Shift"] = "1" as AnyObject
//            tempSectionDict["Shift"] = "1"
//        }
//        else if (shiftOutlet.titleLabel?.text == "Shift B"){
//             param["Shift"] = "2" as AnyObject
//            tempSectionDict["Shift"] = "2"
//
//        }
//        else if (shiftOutlet.titleLabel?.text == "Shift C"){
//             param["Shift"] = "3" as AnyObject
//            tempSectionDict["Shift"] = "3"
//
//        }
         let shiftID  = String(describing: self.shiftId)
         param["Shift"] = shiftID as AnyObject
         tempSectionDict["Shift"] = shiftID
            param["TotalProduction"] = txtProduction.text  as AnyObject
            tempSectionDict["TotalProduction"] = txtProduction.text

            param["TotalManpower"] = sectionParam["TotalManpower"] as AnyObject
            tempSectionDict["TotalManpower"] = sectionParam["TotalManpower"] as? String

            param["Present"] = sectionParam["Present"]  as AnyObject
            tempSectionDict["Present"] = sectionParam["Present"] as? String

            param["Absent"] = sectionParam["Absent"] as AnyObject
            tempSectionDict["Absent"] = sectionParam["Absent"] as? String

            param["Reportable"] = sectionParam["Reportable"]  as AnyObject
            tempSectionDict["Reportable"] = sectionParam["Reportable"] as? String

            param["NonReportable"] = sectionParam["NonReportable"] as AnyObject
            tempSectionDict["NonReportable"] = sectionParam["NonReportable"] as? String

            param["FirstAidInjury"] = sectionParam["FirstAidInjury"] as AnyObject
            tempSectionDict["FirstAidInjury"] = sectionParam["FirstAidInjury"] as? String

            param["NearMiss"] = sectionParam["NearMiss"] as AnyObject
            tempSectionDict["NearMiss"] = sectionParam["NearMiss"] as? String

            param["Suggestions"] = sectionParam["Suggestions"]  as AnyObject
            tempSectionDict["Suggestions"] = sectionParam["Suggestions"] as? String

            param["ProductionIssues"] = sectionParam["ProductionIssues"] as AnyObject
            tempSectionDict["ProductionIssues"] = sectionParam["ProductionIssues"] as? String

            param["QualityIssues"] = sectionParam["QualityIssues"]  as AnyObject
            tempSectionDict["QualityIssues"] = sectionParam["QualityIssues"] as? String

            param["SafetyIssues"] = sectionParam["SafetyIssues"] as AnyObject
            tempSectionDict["SafetyIssues"] = sectionParam["SafetyIssues"] as? String
            param["IsEdit"] = isEditBool  as AnyObject
             tempSectionDict["IsEdit"] = isEditBool
            param["DetailId"] = detailID as AnyObject
            param["lstPlanning"] = requestPlanningData
            view.lock(tintColor: Constant.appThemeColor)
            let dict = Json4Swift_Base.init(dictionary: param)
            self.sectionPlanningData = [String:AnyObject] ()
            self.sectionPlanningData = tempSectionDict as [String : AnyObject]
            AddDetailViewModel.onClicKSaveDetail(detail: param as [String : AnyObject]) { (response, eror) in
                print(response!)
                self.view.unlock()
                self.view.makeToast(response)
                self.planningModel =
                    LstPlanning.modelsFromDictionaryArray(array: self.lstPlanningData as [[String: Any]])
                self.otherSectionModels = Json4Swift_Base.modelsFromDictionaryArray(array: [self.sectionPlanningData])

                self.delegate?.childViewControllerResponse(detail: self.otherSectionModels, plan: self.planningModel)
                self.navigationController?.popViewController(animated: true)
            }
        }
            
        else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        view.endEditing(true)
        // handling code
    }
    func setButtonDropDown() {
        
        view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: shiftOutlet.bounds.height)
        dropDown.anchorView = shiftOutlet
        
        var listNameArray = [String]()
        
        if (Constant.listNameArray.count > 0){
            dropDown.dataSource = Constant.listNameArray
        }
        else {
            setShiftAndModel()
            return
        }

                //dropDown.dataSource = listNameArray
                dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                    self.shiftOutlet.setTitle(item, for: .normal)
                    // self.currentTxt.text = item
                    self.shiftId = index + 1
                    if self.currentTxt == self.txtShift {
                       
                    }
                    dropDown.hide()
                }
                dropDown.show()
            
    }
    
    func setShiftAndModel(){
        
        ViewDetailViewModel.getAllShifts(completionHandler: { (response, error) in
            for component in response {
                let shift = component["Shift"] as! String
                Constant.listNameArray.append(shift)
            }
        })
    }
    func setBackViewValues(){
        if (Constant.modelsNameArray.count > 0){
            backViewModel.setTitle(Constant.modelsNameArray[0] as String , for: .normal)
            self.modelId = 1
        }
        else {
             backViewModel.setTitle(""  , for: .normal)
            self.modelId = 0
        }
        
        backViewModel.layer.borderWidth = 1
        backViewModel.layer.borderColor = UIColor.black.cgColor
        fromChesisTxt.text = ""
        toChessisTxt.text = ""
        qtyTxt.text = ""
        backViewStatus.setTitle("Yes", for: .normal)
        backViewStatus.layer.borderWidth = 1
        backViewStatus.layer.borderColor = UIColor.black.cgColor
        statusTxt.text = ""
    }
    
    
    func setEditData() {
        if (planningModel != nil){
            if let plans = planningModel as? [LstPlanning] {
                for plan in plans {
                    var  templstPlanningData = [String:AnyObject]()
                   
//                    let shift = plan.model
//                    let array = shift?.components(separatedBy: " ")
//                    if (array?.count == 2){
//                        templstPlanningData["Model"] = array?[1] as AnyObject
//                       // templstPlanningData["ModelValue"] = plan.model
//                    }
                    templstPlanningData["Model"] = plan.model as AnyObject
                    templstPlanningData["ModelId"] = plan.ModelId as AnyObject
                    templstPlanningData["FromChassis"] = plan.fromChassis as AnyObject
                    templstPlanningData["ToChassis"] = plan.toChassis as AnyObject
                    templstPlanningData["Quantity"] = plan.quantity as AnyObject
                    templstPlanningData["Status"] = plan.status as AnyObject
                    lstPlanningData.append(templstPlanningData)
                }
            }
        }
        
        if (otherSectionModels != nil){
            if let sections = otherSectionModels as? [Json4Swift_Base] {
                for section in sections {
                    
                   txtDate.text = section.date
                    if (section.shift == "1"){
                        shiftOutlet.setTitle("Shift A", for: .normal)
                    }
                    else if (section.shift == "2"){
                        shiftOutlet.setTitle("Shift B", for: .normal)
                    }
                    else {
                        shiftOutlet.setTitle(" Shift C", for: .normal)
                    }
                    let id = section.shift ?? ""
                    
                    self.shiftId = Int(id)!
                    txtProduction.text = section.totalProduction
                    addTtitle.text = "Edit Detail"
                    saveBtnTitle.setTitle("Update", for: .normal)
                    var  templstPlanningData = [String:String]()
                    templstPlanningData["Date"] = section.date
                    templstPlanningData["Shift"] = section.shift
                    templstPlanningData["TotalProduction"] = section.totalProduction
                    templstPlanningData["TotalManpower"] = section.totalManpower
                    templstPlanningData["Present"] = section.present
                    templstPlanningData["Absent"] = section.absent
                    templstPlanningData["Reportable"] = section.reportable
                    templstPlanningData["NonReportable"] = section.nonReportable
                    templstPlanningData["FirstAidInjury"] = section.firstAidInjury
                    templstPlanningData["NearMiss"] = section.nearMiss
                    templstPlanningData["Suggestions"] = section.suggestions
                    templstPlanningData["ProductionIssues"] = section.productionIssues
                    templstPlanningData["QualityIssues"] = section.qualityIssues
                    templstPlanningData["SafetyIssues"] = section.safetyIssues
                    sectionParam = templstPlanningData
                    sectionPlanningData = templstPlanningData as [String : AnyObject]
                }
               self.otherSectionModels =  Json4Swift_Base.modelsFromDictionaryArray(array: [sectionPlanningData])
                txtDate.isUserInteractionEnabled = false
                shiftOutlet.isUserInteractionEnabled = false
               
            }
        }
    }
    
    func setUpInitialView() {
        txtDate.rightView(withImage: "calendar", tintColor: .darkGray)
        txtDate.layer.borderWidth = 1
        txtDate.layer.borderColor = UIColor.black.cgColor
        
        if (Constant.listNameArray.count > 0){
           shiftOutlet.setTitle(Constant.listNameArray[0] as String, for: .normal)
            self.shiftId = 1
        }
        else {
            shiftOutlet.setTitle("", for: .normal)
            self.shiftId = 0
        }
        shiftOutlet.layer.cornerRadius = 5
        shiftOutlet.layer.borderWidth = 1
        shiftOutlet.layer.borderColor = UIColor.black.cgColor
        
        txtProduction.layer.borderWidth = 1
        txtProduction.layer.borderColor = UIColor.black.cgColor
        
        
    }
     @objc func buttonAction(_ sender: UIButton){
        
        view.endEditing(true)
        let cell: UITableViewCell = sender.superview?.superview?.superview as! UITableViewCell
        let table: UITableView = cell.superview as! UITableView
        let textFieldIndexPath = table.indexPath(for: cell)
        if (textFieldIndexPath?.section == 1){
            var textFieldTags = Int()
            textFieldTags =  sender.tag / 10
            let reminder =  sender.tag.quotientAndRemainder(dividingBy: 10).remainder
            
            // cell 0 --- deleteBtn 6
            // cell 1 --- deleteBtn 16
            // cell 2 --- deleteBtn 26
            // cell 3 --- deleteBtn 36
            
            if(sender.tag > 9){
                cellIndex = sender.tag.quotientAndRemainder(dividingBy: 10).quotient
                addbtnTag.tag = 0
                
                if (reminder == 1){
                    //dropDown()
                }
                if reminder == 6 {
                     deleteCellAction(indexOfCell: cellIndex)
                }
                else {
                    addView()
                }
            }
            else{
                textFieldTags = sender.tag
                cellIndex   =   0
                addbtnTag.tag = 0
                
                if (reminder == 1){
                    // dropDown()
                }
                if reminder == 6 {
                    deleteCellAction(indexOfCell: cellIndex)
                }
                else {
                    addView()
                }
                
                
            }
        }
    }
    
    func deleteCellAction(indexOfCell: Int) {
        
        self.lstPlanningData.remove(at: indexOfCell)
        tableView.reloadData()
    }
    
    func addView() {
        DispatchQueue.main.async {
            self.view.endEditing(true)
            self.backView.isHidden = false
            self.backView.frame = CGRect (x: 0, y:0, width: self.view.frame.size.width , height: self.view.frame.size.height)
            self.backView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
            
            self.addRowView.center = self.backView.center
            self.view.addSubview(self.backView)
            self.backView.addSubview(self.addRowView)
            self.view.bringSubviewToFront(self.addRowView)
            
            if (self.lstPlanningData.count > 0){
                let string1 = "Model"
                self.modelId  = self.lstPlanningData[self.cellIndex]["ModelId"] as! Int
                // var model = "\(String(describing: string1))\(" ")\(string2)"
                self.backViewModel.setTitle(self.lstPlanningData[self.cellIndex]["Model"] as? String, for: .normal)
                self.modelId = self.lstPlanningData[self.cellIndex]["ModelId"] as! Int
                self.fromChesisTxt.text = self.lstPlanningData[self.cellIndex]["FromChassis"] as? String
                self.toChessisTxt.text = self.lstPlanningData[self.cellIndex]["ToChassis"] as? String
                self.qtyTxt.text = self.lstPlanningData[self.cellIndex]["Quantity"] as? String
                self.backViewStatus.setTitle(self.lstPlanningData[self.cellIndex]["Status"] as? String, for: .normal)
            }
        }
    }
}
