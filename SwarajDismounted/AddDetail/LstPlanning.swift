//  Created by Gurwinder Singh on 5/2/19.



import Foundation
 

public class LstPlanning: NSObject {
	public var model : String?
    public var modelValue : String?
    public var ModelId : Int?
	public var fromChassis : String?
	public var toChassis : String?
	public var quantity : String?
	public var status : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let lstPlanning_list = LstPlanning.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of LstPlanning Instances.
*/
    public class func modelsFromDictionaryArray(array:[[String:Any]]) -> [LstPlanning]
    {
        var models:[LstPlanning] = []
        for item in array
        {
           
            models.append(LstPlanning(dictionary: item as [String:AnyObject])!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let lstPlanning = LstPlanning(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: LstPlanning Instance.
*/
	 public init?(dictionary: [String:AnyObject]) {
         model = dictionary["Model"] as? String
       // modelValue = dictionary["ModelValue"] as? String
        ModelId = dictionary["ModelId"] as? Int
        fromChassis = dictionary["FromChassis"] as? String
        toChassis = dictionary["ToChassis"] as? String
        quantity = dictionary["Quantity"] as? String
        status = dictionary["Status"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> [String:AnyObject] {

		var dictionary = [String:AnyObject]()
        
        dictionary["Model"] = self.model as AnyObject
        // dictionary["ModelValue"]  = self.modelValue
        dictionary["FromChassis"] = self.fromChassis as AnyObject
        dictionary["ModelId"] = self.ModelId as AnyObject
        dictionary["ToChassis"] = self.toChassis as AnyObject
        dictionary["Quantity"] = self.quantity as AnyObject
        dictionary["Status"] = self.status as AnyObject
//
//        dictionary.setValue(self.model, forKey: "Model")
//        dictionary.setValue(self.fromChassis, forKey: "FromChassis")
//        dictionary.setValue(self.toChassis, forKey: "ToChassis")
//        dictionary.setValue(self.quantity, forKey: "Quantity")
//        dictionary.setValue(self.status, forKey: "Status")

		return dictionary
	}

}
