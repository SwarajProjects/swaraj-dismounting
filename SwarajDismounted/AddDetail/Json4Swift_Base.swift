
//  Created by Gurwinder Singh on 5/2/19.


import Foundation
import UIKit
 
public class Json4Swift_Base : NSObject{
	public var userId : String?
	public var date : String?
	public var shift : String?
	public var totalProduction : String?
	public var totalManpower : String?
	public var present : String?
	public var absent : String?
	public var reportable : String?
	public var nonReportable : String?
	public var firstAidInjury : String?
	public var nearMiss : String?
	public var suggestions : String?
	public var productionIssues : String?
	public var qualityIssues : String?
	public var safetyIssues : String?
	public var isEdit : Bool?
	public var detailId : Int?
	public var lstPlanning : [LstPlanning]!

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Json4Swift_Base Instances.
*/
    public class func modelsFromDictionaryArray(array:[[String:AnyObject]]) -> [Json4Swift_Base]
    {
        var models:[Json4Swift_Base] = []
        for item in array
        {
            models.append(Json4Swift_Base(dictionary: item as! [String: Any])!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Json4Swift_Base Instance.
*/
	required public init?(dictionary: [String:Any]) {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "MM/dd/yyyy"
       
		userId = dictionary["UserId"] as? String
        let dateSeparated = dictionary["Date"] as? String
        let actualDate = dateSeparated?.components(separatedBy: " ")
        let showDate = inputFormatter.date(from:actualDate?[0] ?? "")
        if (showDate != nil){
            inputFormatter.dateFormat = "dd/MM/yyyy"
            let resultString = inputFormatter.string(from: showDate!)
            date = resultString as? String
        }
        else{
            date = actualDate?[0] as? String
        }
		shift = dictionary["Shift"] as? String
		totalProduction = dictionary["TotalProduction"] as? String
		totalManpower = dictionary["TotalManpower"] as? String
		present = dictionary["Present"] as? String
		absent = dictionary["Absent"] as? String
		reportable = dictionary["Reportable"] as? String
		nonReportable = dictionary["NonReportable"] as? String
		firstAidInjury = dictionary["FirstAidInjury"] as? String
		nearMiss = dictionary["NearMiss"] as? String
		suggestions = dictionary["Suggestions"] as? String
		productionIssues = dictionary["ProductionIssues"] as? String
		qualityIssues = dictionary["QualityIssues"] as? String
		safetyIssues = dictionary["SafetyIssues"] as? String
		isEdit = dictionary["IsEdit"] as? Bool
		detailId = dictionary["Id"] as? Int
        lstPlanning = [LstPlanning]()
        
//            if let lstPlanningArray = dictionary["lstPlanning"] as? [[String:String]]{
//                for dic in lstPlanningArray{
//                    let value = LstPlanning(dictionary: dic as [String : String])
//                    lstPlanning.append(value!)
//                }
//            }
            //lstPlanning =
            //LstPlanning.modelsFromDictionaryArray(array: dictionary["lstPlanning"] as! [[String: String]])
            
        
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> [String:Any] {

        var dictionary = [String:Any]()

        dictionary["UserId"] = self.userId
        dictionary["Date"] = self.date
        dictionary["Shift"] = self.shift
        dictionary["TotalProduction"] = self.totalProduction
        dictionary["totalManpower"] = self.totalManpower
        dictionary["Present"] = self.present
        dictionary["Reportable"] = self.reportable
        dictionary["NonReportable"] = self.nonReportable
        dictionary["FirstAidInjury"] = self.firstAidInjury
        dictionary["ProductionIssues"] = self.productionIssues
        dictionary["Suggestions"] = self.suggestions
        dictionary["QualityIssues"] = self.qualityIssues
        dictionary["SafetyIssues"] = self.safetyIssues
        dictionary["IsEdit"] = self.isEdit
        dictionary["Id"] = self.detailId


		return dictionary
	}

}
