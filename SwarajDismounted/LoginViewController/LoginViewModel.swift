//
//  LoginViewModel.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/1/19.
//

import UIKit

class LoginViewModel: NSObject {
    
    func loginValidations(user: User) -> (String, Bool) {
        
        var error = ""
        
        if user.username?.isEmpty == true{
            error = StringConstant.SignIn.emptyTokenNumber
        }
        else if user.password?.isEmpty == true{
            error = StringConstant.SignIn.emptyPassword
        }
        
        if error.isEmpty == true{
            return(error, true)
        } else {
            return(error, false)
        }
        
    }
}
