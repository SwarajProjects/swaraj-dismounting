//
//  User.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/1/19.
//

import UIKit

class User: NSObject {
    
    var userId: Int?
    var username: String?
    var password: String?
    var emailID: String?
    var name : String?
    var isEditRights: Int?
    
    init(dict: [String: Any] = [:]) {
        
        userId = dict["Id"] as? Int
        username = dict["Username"] as? String
        password = dict["Password"] as? String
        emailID = dict["Email"] as? String
        name = dict["Name"] as? String
        isEditRights = dict["isEditRights"] as? Int
        
    }
}
