//
//  ViewController.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/1/19.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: - Properties
    let loginViewModel = LoginViewModel()
    
    // MARK: - Outlets
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var bottomConstant: NSLayoutConstraint!
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    
    // MARK: - View life cycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        txtUserName.text = "25002065"
        //        textPassword.text = "25002065"
        setUpInitialViews()
        Constant.listNameArray = [String]()
        Constant.modelsNameArray = [String]()
        
    }
    
    func setUpInitialViews() {
        animation(viewAnimation: loginView, constant: -10)
        
        txtUserName.bottomLine(lineColor: UIColor.lightGray)
        textPassword.bottomLine(lineColor: UIColor.lightGray)
        
        txtUserName.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        textPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        
        txtUserName.leftView(withImage: "user", tintColor: .darkGray)
        textPassword.leftView(withImage: "password", tintColor: .darkGray)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        animation(viewAnimation: loginView, constant: -7)
    }
    
    private func animation(viewAnimation: UIView, constant: CGFloat) {
        
        UIView.animate(withDuration: 1, delay: 0.3, options: [], animations: {
            self.bottomConstant.constant = constant
            viewAnimation.frame.origin.y = viewAnimation.frame.origin.y - 304
            
        }, completion: { _ in
        })
        
    }
    
    @IBAction func onClickBtn(_ sender: Any) {
        
        view.endEditing(true)
        view.lock(tintColor: Constant.appThemeColor)
        let loginDict = ["userName": txtUserName.text ?? "", "password": textPassword.text ?? ""]
        
        let user = User(dict: loginDict)
        let (errorMessage, status) = loginViewModel.loginValidations(user: user)
        
        if status == false {
            self.view.makeToast(errorMessage)
        }
        else {
            if isInternetActive {
                loginInUser(user: user)
            }
            else {
                self.view.makeToast(StringConstant.noInternet)
            }
            
        }
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem ?? "", attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

