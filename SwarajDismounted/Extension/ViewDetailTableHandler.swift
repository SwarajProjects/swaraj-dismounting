//
//  ViewDetailTableHandler.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/6/19.
//

import Foundation
import UIKit

extension ViewDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if (planningModel == nil && otherSectionModels == nil){
            tableView.backgroundView = backLbel
        }
        if (section == 1){
            if(planningModel == nil) {
                return 0
            }
            return planningModel.count
        }
        else {
            if(otherSectionModels == nil) {
                return 0
            }
            return self.otherSectionModels.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section == 0){
            return 50
        }
        else if (indexPath.section == 1){
            return 50
        }
        
        if (indexPath.section == 2){
            return 210
        }
        else if (indexPath.section == 3){
            return 270
        }
            
        else {
            return 350
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.ViewDetailPlanningCell, for: indexPath) as! ViewDetailPlanningCell
            
            
            return cell
        }
            
        else if (indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.sectionOneCell, for: indexPath) as! sectionOneCell
            let plan = planningModel[indexPath.row]
            print(plan)
            
            if (indexPath.row % 2) == 0 {

                cell.modelTxt.backgroundColor = Colors.colorWithRGB(r: 226, g: 249, b: 247)
                cell.fromTxt.backgroundColor = Colors.colorWithRGB(r: 226, g: 249, b: 247)
                cell.toText.backgroundColor = Colors.colorWithRGB(r: 226, g: 249, b: 247)
                cell.qtyTxt.backgroundColor = Colors.colorWithRGB(r: 226, g: 249, b: 247)
                cell.statusTxt.backgroundColor = Colors.colorWithRGB(r: 226, g: 249, b: 247)

            } else {

                cell.modelTxt.backgroundColor = Colors.colorWithRGB(r: 190, g: 231, b: 226)
                cell.fromTxt.backgroundColor = Colors.colorWithRGB(r: 190, g: 231, b: 226)
                cell.toText.backgroundColor = Colors.colorWithRGB(r: 190, g: 231, b: 226)
                cell.qtyTxt.backgroundColor = Colors.colorWithRGB(r: 190, g: 231, b: 226)
                cell.statusTxt.backgroundColor = Colors.colorWithRGB(r: 190, g: 231, b: 226)
            }
            let string1 = "Model"
            let string2 =  plan.model
            let model = "\(String(describing: string1))\(" ")\(String(describing: string2))"
            cell.modelTxt.text = plan.model
            cell.fromTxt.text = plan.fromChassis ?? ""
            cell.toText.text = plan.toChassis ?? ""
            cell.qtyTxt.text = plan.quantity ?? ""
            cell.statusTxt.text = plan.status ?? ""
            
            return cell
        }
        else if (indexPath.section == 2) {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.ViewAbsentiesmCell, for: indexPath) as! ViewAbsentiesmCell
            if (otherSectionModels != nil){
                let absentail = self.otherSectionModels[indexPath.row]
                cell.totalManpowerTxt.text = absentail.totalManpower ?? ""
                cell.presentTxt.text = absentail.present ?? ""
                cell.absentTxt.text = absentail.absent ?? ""
            }
            else {
                cell.totalManpowerTxt.text = ""
                cell.presentTxt.text = ""
                cell.absentTxt.text = ""
            }
           
            
            return cell
        }
        else if (indexPath.section == 3) {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.SafetyCell, for: indexPath) as! SafetyCell
            if (otherSectionModels != nil){
                let safety = self.otherSectionModels[indexPath.row]
                cell.reportableTxt.text = safety.reportable ?? ""
                cell.nonReportableTxt.text = safety.nonReportable ?? ""
                cell.firstAudTxt.text = safety.firstAidInjury ?? ""
                cell.nearMiss.text = safety.nearMiss ?? ""
                cell.suggestionTxt.text = safety.suggestions ?? ""
            }
            else {
                cell.reportableTxt.text =  ""
                cell.nonReportableTxt.text =  ""
                cell.firstAudTxt.text =  ""
                cell.nearMiss.text =  ""
                cell.suggestionTxt.text = ""
            }
         
            return cell
        }
        else  {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.ViewAssembleyCell, for: indexPath) as! ViewAssembleyCell
            if (otherSectionModels != nil){
                let assembly = self.otherSectionModels[indexPath.row]
                cell.productionTxt.text = assembly.productionIssues ?? ""
                cell.qualityTxt.text = assembly.qualityIssues ?? ""
                cell.safetyTxt.text = assembly.safetyIssues ?? ""
            }
            else {
                cell.productionTxt.text =  ""
                cell.qualityTxt.text =  ""
                cell.safetyTxt.text =  ""
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if ((planningModel == nil) && (otherSectionModels == nil)){
            let emptyView = UIView()
            return emptyView
        }
        
        if (section == 0){
            
            viewHeaderView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60)
            viewHeaderView.backgroundColor = UIColor(red: 195/255.0, green: 231/255.0, blue: 231/255.0, alpha: 1.0)
            viewTitleLbl.text = "Planning"
            return viewHeaderView
        }
        else if (section == 1){
            let rowView = UIView()
            return rowView
        }
        else if (section == 2){
            let AbsentiesmView = UIView()
            let AbsentiesmLabel = UILabel()
            AbsentiesmLabel.font = UIFont.boldSystemFont(ofSize: 19)
            AbsentiesmLabel.frame = CGRect(x: 10, y: 0, width: 200, height: 50)
            AbsentiesmView.backgroundColor = UIColor(red: 195/255.0, green: 231/255.0, blue: 231/255.0, alpha: 1.0)
            AbsentiesmView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60)
            AbsentiesmLabel.text = "Absentiesm"
            AbsentiesmView.addSubview(AbsentiesmLabel)
            return AbsentiesmView
        }
            
        else if (section == 3){
            let SafetyView = UIView()
            let SafetyLabel = UILabel()
            SafetyLabel.font = UIFont.boldSystemFont(ofSize: 19)
            SafetyLabel.frame = CGRect(x: 10, y: 0, width: 200, height: 50)
            SafetyView.backgroundColor = UIColor(red: 195/255.0, green: 231/255.0, blue: 231/255.0, alpha: 1.0)
            SafetyView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60)
            SafetyLabel.text = "Safety"
            SafetyView.addSubview(SafetyLabel)
            return SafetyView
        }
        else {
            let ASSEMBLYView = UIView()
            let ASSEMBLYLabel = UILabel()
            ASSEMBLYLabel.font = UIFont.boldSystemFont(ofSize: 19)
            ASSEMBLYLabel.frame = CGRect(x: 10, y: 0, width: 320, height: 50)
            ASSEMBLYView.backgroundColor = UIColor(red: 195/255.0, green: 231/255.0, blue: 231/255.0, alpha: 1.0)
            ASSEMBLYView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60)
            //            titleLbl.text = "ASSEMBLY ISSUES (PCQDCM)"
            ASSEMBLYLabel.text = "ASSEMBLY ISSUES (PCQDCM)"
            ASSEMBLYView.addSubview(ASSEMBLYLabel)
            return ASSEMBLYView
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if (planningModel != nil && otherSectionModels != nil){
            if (section == 1){
                return 5
            }
            else {
              return 50
            }
        }
        return 0
    }
}
