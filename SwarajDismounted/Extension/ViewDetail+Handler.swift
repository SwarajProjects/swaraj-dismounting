//
//  ViewDetail+Handler.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/2/19.
//

import Foundation
import UIKit
import DropDown


extension ViewDetailsViewController: UITextFieldDelegate, GetDate{
   
    func getDateFromCalender(date: String, machineId: String) {
        currentTxt.text = date
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTxt = textField
        
        if textField == txtDate {
            
            if let calendarVC = Constant.Storyboards.main.instantiateViewController(withIdentifier: Constant.StoryBoardIDs.calendarVC) as? CalendarViewController {

                calendarVC.selectedDateDelegate = self
                calendarVC.isFilterView = false
                self.present(calendarVC, animated: true, completion: nil)
            }
            return false
            
        } else if textField == txtShift {
            dropDown()
            
            return true
        }
        else if (textField == txtProduction){
            
        }
        return true
    }
    
    func dropDown() {
        
        view.endEditing(true)
                let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: currentTxt.bounds.height)
        dropDown.anchorView = currentTxt
        
        var listNameArray = [String]()
        
        if currentTxt == txtShift {
            
            ViewDetailViewModel.getAllShifts(completionHandler: { (response, error) in
                for component in response {
                    let shift = component["Shift"] as! String
                    let array = shift.components(separatedBy: " ")
                    if (array.count > 0){
                        listNameArray.append( array[1])
                    }
                }
                dropDown.dataSource = listNameArray
                dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                    
                    self.currentTxt.text = item
                    
                    if self.currentTxt == self.txtShift {
                        self.shiftId = index + 1
                    }
                    dropDown.hide()
                }
                dropDown.show()
            })
           
        }
       
    }
    
}

