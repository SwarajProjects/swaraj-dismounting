//
//  LoginViewController.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/1/19.
//

import Foundation
import UIKit

extension LoginViewController {
    
    func loginInUser(user: User) {
        
        view.lock(tintColor: Constant.appThemeColor)
        let loginDict = ["Username": txtUserName.text!,
                         "Password" : textPassword.text!] as [String : Any]
        
        NetworkProvider.shared.request(Constant.baseURL + Constant.WebServiceURL.getUserDetailsForLogin, requestDict: loginDict as [String : AnyObject], method: .post, headers: nil, compBlock: { (responseDict) in
            let responseCode = responseDict["StatusCode"] as? Int ?? 0
            self.view.unlock()
            
            if (responseDict["Message"] as? String ?? "") == "Invalid credentials" {
                self.view.makeToast(responseDict["Message"] as? String)
            } else if (responseDict["Message"] as? String ?? "") == "Success" {
                if let userDetailDict = responseDict["ResultData"] as? [[String:Any]]
                    ,let user = userDetailDict.first {
                    
                    Constant.userData = User(dict: user)
                    UserDefaults.standard.removeObject(forKey: Constant.UserDefaults.userDetailDict)
                    let userDetail = NSKeyedArchiver.archivedData(withRootObject: user as Any)
                    UserDefaults.standard.set(userDetail, forKey: Constant.UserDefaults.userDetailDict)
                    
                    if let viewDetailsVC = Constant.Storyboards.main.instantiateViewController(withIdentifier: Constant.StoryBoardIDs.ViewDetailsViewController) as? ViewDetailsViewController {
                        viewDetailsVC.user = User(dict: user)
                        self.navigationController?.pushViewController(viewDetailsVC, animated: true)
                    }
                }
            }
        }) { (error) in
            self.view.unlock()
        }
        
    }
}
