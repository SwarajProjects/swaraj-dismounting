//
//  AddDetail+TxtDatasources.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/3/19.
//

import Foundation
import UIKit
import DropDown

extension AddDetailViewController :UITextFieldDelegate, UITextViewDelegate{
    
    func setYesNoDropDown() {
        
        view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: backViewStatus.bounds.height)
        dropDown.anchorView = backViewStatus
        
        var listNameArray = [String]()
        
            for component in yesNoArray {
                let shift = component["selection"] as! String
                listNameArray.append(shift)
        }
        
            dropDown.dataSource = listNameArray
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.backViewStatus.setTitle(item, for: .normal)
                // self.currentTxt.text = item
                
                if self.currentTxt == self.txtShift {
                    self.shiftId = index + 1
                }
                dropDown.hide()
            }
            dropDown.show()
        
    }
    func idBackSpace (string : String) -> Bool {
        if let char = string.cString(using: String.Encoding.utf8){
            let isbackSpace = strcmp(char, "\\b")
            if isbackSpace == -92{
                return true
            }
        }
        return false
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        currentTxtView = textView
        
        if ((textView.superview?.superview?.isKind(of: UITableViewCell.self))!){
            var cell: UITableViewCell = textView.superview?.superview as! UITableViewCell
            var table: UITableView = textView.superview?.superview?.superview as! UITableView
            let textFieldIndexPath = table.indexPath(for: cell)
            if (textFieldIndexPath?.section == 4){
                
                var textFieldTags = Int()
                textFieldTags =  textView.tag / 40
                let reminder =  textView.tag.quotientAndRemainder(dividingBy: 40).remainder
                // var cellIndex = Int()
                cellIndex = textView.tag.quotientAndRemainder(dividingBy: 40).quotient
                if (reminder == 1){
                    
                    let backspace = self.idBackSpace(string: text)
                    if backspace == true && textView.text.count == 1{
                        sectionParam["ProductionIssues"] = ""
                    }
                    else if (textView.text != ""  && backspace == true){
                       currentTxtView.text.removeLast()
                        sectionParam["ProductionIssues"] = currentTxtView.text
                    }
                    else {
                        if (textView.text != "" ){
                            sectionParam["ProductionIssues"] = "\(String(describing: currentTxtView.text!))\(text)"
                        }
                        else {
                            sectionParam["ProductionIssues"] = text
                        }
                    }
                }
                else if (reminder == 2){
                    
                    
                    let backspace = self.idBackSpace(string: text)
                    if backspace == true && textView.text.count == 1{
                        sectionParam["SafetyIssues"] = ""
                    }
                    else if (textView.text != ""  && backspace == true){
                        currentTxtView.text.removeLast()
                        sectionParam["SafetyIssues"] = currentTxtView.text
                    }
                    else {
                        if (textView.text != "" ){
                            sectionParam["SafetyIssues"] = "\(String(describing: currentTxtView.text!))\(text)"
                        }
                        else {
                            sectionParam["SafetyIssues"] = text
                        }
                    }
                }
                else if (reminder == 3){
                    let backspace = self.idBackSpace(string: text)
                    if backspace == true && textView.text.count == 1{
                        sectionParam["QualityIssues"] = ""
                    }
                    else if (textView.text != ""  && backspace == true){
                        currentTxtView.text.removeLast()
                        sectionParam["QualityIssues"] = currentTxtView.text
                    }
                    else {
                        
                        if (textView.text != "" ){
                            sectionParam["QualityIssues"] = "\(String(describing: currentTxtView.text!))\(text)"
                        }
                        else {
                            sectionParam["QualityIssues"] = text
                        }
                    }

                }
            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
        currentTxt = textField
        //modelTxt
        if (textField.tag == 90){
            
            dropDown()
        }
        else if (textField.tag == 91){
        }
       else if (textField.tag == 92){
        }
       else if (textField.tag == 93){
        }
       else if (textField.tag == 94){
        }
       
       else if textField == txtDate {
            
            if let calendarVC = Constant.Storyboards.main.instantiateViewController(withIdentifier: Constant.StoryBoardIDs.calendarVC) as? CalendarViewController {
                
                calendarVC.selectedDateDelegate = self
                calendarVC.isFilterView = false
                self.present(calendarVC, animated: true, completion: nil)
            }
            return false
        }
        else if textField == txtProduction {
            
            return true
            }
            
        
        else {
            if ((currentTxt.superview?.superview?.isKind(of: UITableViewCell.self))!){
            }
            else {
                let cell: UITableViewCell = currentTxt.superview?.superview?.superview as! UITableViewCell
                let table: UITableView = cell.superview as! UITableView
                let textFieldIndexPath = table.indexPath(for: cell)
                if (textFieldIndexPath?.section == 1){
                    var textFieldTags = Int()
                    textFieldTags =  currentTxt.tag / 10
                    let reminder =  currentTxt.tag.quotientAndRemainder(dividingBy: 10).remainder
                    
                    if(textField.tag > 9){
                        if (reminder == 1){
                            //dropDown()
                        }
                        cellIndex = textField.tag.quotientAndRemainder(dividingBy: 10).quotient
                        addbtnTag.tag = 0
                        addView()
                        return true
                    }
                    else{
                        
                        if (reminder == 1){
                           // dropDown()
                        }
                        textFieldTags = textField.tag
                        cellIndex   =   0
                        addbtnTag.tag = 0
                        addView()
                        return true
                    }
                }
            }
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentTxt = textField
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        currentTxt = textField
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        currentTxt = textField
        
        if ((currentTxt.superview?.superview?.isKind(of: UITableViewCell.self))!){
            var cell: UITableViewCell = currentTxt.superview?.superview as! UITableViewCell
            var table: UITableView = currentTxt.superview?.superview?.superview as! UITableView
            let textFieldIndexPath = table.indexPath(for: cell)
            if (textFieldIndexPath?.section == 2){
                var textFieldTags = Int()
                textFieldTags =  currentTxt.tag / 20
                let reminder =  currentTxt.tag.quotientAndRemainder(dividingBy: 20).remainder
                //var cellIndex = Int()
                cellIndex = textField.tag.quotientAndRemainder(dividingBy: 20).quotient
                if (reminder == 1){
                    let backspace = self.idBackSpace(string: string)
                    if backspace == true && textField.text?.count == 1{
                        sectionParam["TotalManpower"] = ""
                    }
                    else if (textField.text != ""  && backspace == true){
                        currentTxt.text?.dropLast()
                        sectionParam["TotalManpower"] = currentTxt.text
                    }
                    else {
                        if (currentTxt.text != "" ){
                            sectionParam["TotalManpower"] = "\(String(describing: currentTxt.text!))\(string)"
                        }
                        else {
                            sectionParam["TotalManpower"] = string
                        }
                    }
                }
                else if (reminder == 2){
                    let backspace = self.idBackSpace(string: string)
                    if backspace == true && textField.text?.count == 1{
                        sectionParam["Present"] = ""
                    }
                    else if (textField.text != ""  && backspace == true){
                       let value = currentTxt.text?.dropLast()
                        sectionParam["Present"] = value
                    }
                    else {
                        if (currentTxt.text != "" ){
                            sectionParam["Present"] = "\(String(describing: currentTxt.text!))\(string)"
                        }
                        else {
                            sectionParam["Present"] = string
                        }
                    }
                    
                }
                else if (reminder == 3){
                    let backspace = self.idBackSpace(string: string)
                    if backspace == true && textField.text?.count == 1{
                        sectionParam["Absent"] = ""
                    }
                    else if (textField.text != ""  && backspace == true){
                        currentTxt.text?.dropLast()
                        sectionParam["Absent"] = currentTxt.text
                    }
                    else {
                        if (currentTxt.text != "" ){
                            sectionParam["Absent"] = "\(String(describing: currentTxt.text!))\(string)"
                        }
                        else {
                            sectionParam["Absent"] = string
                        }
                    }
                   
                }
            }
            else if (textFieldIndexPath?.section == 3){
                var textFieldTags = Int()
                textFieldTags =  currentTxt.tag / 30
                let reminder =  currentTxt.tag.quotientAndRemainder(dividingBy: 30).remainder
               // var cellIndex = Int()
                cellIndex = textField.tag.quotientAndRemainder(dividingBy: 30).quotient
                if (reminder == 1){
                    let backspace = self.idBackSpace(string: string)
                    if backspace == true && textField.text?.count == 1{
                        sectionParam["Reportable"] = ""
                    }
                    else if (textField.text != ""  && backspace == true){                        currentTxt.text?.dropLast()
                        sectionParam["Reportable"] = currentTxt.text
                    }
                    else {
                        if (currentTxt.text != "" ){
                            sectionParam["Reportable"] = "\(String(describing: currentTxt.text!))\(string)"
                        }
                        else {
                            sectionParam["Reportable"] = string
                        }
                    }
                }
                else if (reminder == 2){
                    let backspace = self.idBackSpace(string: string)
                    if backspace == true && textField.text?.count == 1{
                        sectionParam["NonReportable"] = ""
                    }
                    else if (textField.text != ""  && backspace == true){
                        currentTxt.text?.dropLast()
                        sectionParam["NonReportable"] = currentTxt.text
                    }
                    else {
                        if (currentTxt.text != "" ){
                            sectionParam["NonReportable"] = "\(String(describing: currentTxt.text!))\(string)"
                        }
                        else {
                            sectionParam["NonReportable"] = string
                        }
                    }
                }
                else if (reminder == 3){
                    let backspace = self.idBackSpace(string: string)
                    if backspace == true && textField.text?.count == 1{
                        sectionParam["FirstAidInjury"] = ""
                    }
                    else if (textField.text != ""  && backspace == true){
                        currentTxt.text?.dropLast()
                        sectionParam["FirstAidInjury"] = currentTxt.text
                    }
                    else {
                        if (currentTxt.text != "" ){
                            sectionParam["FirstAidInjury"] = "\(String(describing: currentTxt.text!))\(string)"
                        }
                        else {
                            sectionParam["FirstAidInjury"] = string
                        }
                    }
                    
                    
                }
                else if (reminder == 4){
                    let backspace = self.idBackSpace(string: string)
                    if backspace == true && textField.text?.count == 1{
                        sectionParam["NearMiss"] = ""
                    }
                    else if (textField.text != ""  && backspace == true){
                        currentTxt.text?.dropLast()
                        sectionParam["NearMiss"] = currentTxt.text
                    }
                    else {
                        if (currentTxt.text != "" ){
                            sectionParam["NearMiss"] = "\(String(describing: currentTxt.text!))\(string)"
                        }
                        else {
                            sectionParam["NearMiss"] = string
                        }
                    }
                   
                }
                else if (reminder == 5){
                    let backspace = self.idBackSpace(string: string)
                    if backspace == true && textField.text?.count == 1{
                        sectionParam["Suggestions"] = ""
                    }
                    else if (textField.text != ""  && backspace == true){
                        currentTxt.text?.dropLast()
                        sectionParam["Suggestions"] = currentTxt.text
                    }
                    else {
                        if (currentTxt.text != "" ){
                            sectionParam["Suggestions"] = "\(String(describing: currentTxt.text!))\(string)"
                        }
                        else {
                            sectionParam["Suggestions"] = string
                        }
                    }
                   
                }
            }
            else {
                var textFieldTags = Int()
                textFieldTags =  currentTxt.tag / 40
                let reminder =  currentTxt.tag.quotientAndRemainder(dividingBy: 40).remainder
               // var cellIndex = Int()
                cellIndex = textField.tag.quotientAndRemainder(dividingBy: 40).quotient
                if (reminder == 1){
                    if (currentTxt.text != "" ){
                        sectionParam["ProductionIssues"] = "\(String(describing: currentTxt.text!))\(string)"
                    }
                    else {
                        sectionParam["ProductionIssues"] = string
                    }
                }
                else if (reminder == 2){
                    if (currentTxt.text != "" ){
                        sectionParam["QualityIssues"] = "\(String(describing: currentTxt.text!))\(string)"
                    }
                    else {
                        sectionParam["QualityIssues"] = string
                    }
                }
                else if (reminder == 3){
                    if (currentTxt.text != "" ){
                        sectionParam["SafetyIssues"] = "\(String(describing: currentTxt.text!))\(string)"
                    }
                    else {
                        sectionParam["SafetyIssues"] = string
                    }
                }
            }
        }
       return true
    }
    
    func updateDataWitTextFields(textField1:String,textField2:String,textfield3:String,textField4:String,textfield5:String) -> [String:String] {
        var dict = [String:String]()
        dict["Model"]       =   textField1
        dict["FromChassis"] =   textField2
        dict["ToChassis"]   =   textfield3
        dict["Quantity"]    =   textField4
        dict["Status"]      =   textfield5
        return dict
        
    }
    
    
}
extension AddDetailViewController: GetDate{
    
    func getDateFromCalender(date: String, machineId: String) {
        currentTxt.text = date
    }
    
    func setModels(){
        AddDetailViewModel.getAllModels(completionHandler: { (response, error) in
            
            if (Constant.modelsNameArray.count <= 0){
                for component in response {
                    var tempModelData = [String:AnyObject]()
                    tempModelData["Model"] = component["Model"] as AnyObject
                    tempModelData["ModelId"] = component["ModelId"] as AnyObject
                    let shift = component["Model"] as! String
                    Constant.modelsNameArray.append(shift)
                    
                }
            }
        })
    }
    func dropDown() {
        
        self.view.endEditing(true)
        let dropDown = DropDown()
        
        dropDown.bottomOffset = CGPoint(x: 0, y: backViewModel.bounds.height)
        dropDown.anchorView = backViewModel
        
        var listNameArray = [String]()
     
            DispatchQueue.main.async {
                if (Constant.modelsNameArray.count > 0){
                    
                    dropDown.dataSource = Constant.modelsNameArray
                }
                    
                else {
                    self.setModels()
                    return
                }
                    dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                        
                        self.backViewModel.setTitle(item, for: .normal)
                        self.modelId = index + 1
                        dropDown.hide()
                    }
                    dropDown.show()
            }
    }
}
