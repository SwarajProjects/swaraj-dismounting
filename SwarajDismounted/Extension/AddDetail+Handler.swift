//
//  AddDetail+Handler.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/2/19.
//

import Foundation
import UIKit
import DropDown
extension AddDetailViewController : UITableViewDelegate, UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 1){
            return lstPlanningData.count
        }
        if (otherSectionModels != nil){
             return otherSectionModels.count
        }
       return 1
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section == 0){
            return 50
        }
        else if (indexPath.section == 1){
            if (lstPlanningData.count > 0){
               return 60
            }
            return 0
        }
        
        if (indexPath.section == 2){
           return 210
        }
       else if (indexPath.section == 3){
            return 270
        }
            
        else {
            return 350
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.headerViewTableViewCell, for: indexPath) as! HeaderViewTableViewCell
            return cell
        }
        
       else if (indexPath.section == 1){
             let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.planningCellIdentifier, for: indexPath) as! PlanningTableViewCell
            let dataOfPlanning = lstPlanningData[indexPath.row]
            if (indexPath.row % 2) == 0 {
                cell.modeBtn.backgroundColor = Colors.colorWithRGB(r: 226, g: 249, b: 247)
                cell.fromChesisBtn.backgroundColor = Colors.colorWithRGB(r: 226, g: 249, b: 247)
                cell.toChessis.backgroundColor = Colors.colorWithRGB(r: 226, g: 249, b: 247)
                cell.qtyBtn.backgroundColor = Colors.colorWithRGB(r: 226, g: 249, b: 247)
                cell.statusBtn.backgroundColor = Colors.colorWithRGB(r: 226, g: 249, b: 247)
                
            } else {
                cell.modeBtn.backgroundColor = Colors.colorWithRGB(r: 190, g: 231, b: 226)
                cell.fromChesisBtn.backgroundColor = Colors.colorWithRGB(r: 190, g: 231, b: 226)
                cell.toChessis.backgroundColor = Colors.colorWithRGB(r: 190, g: 231, b: 226)
                cell.qtyBtn.backgroundColor = Colors.colorWithRGB(r: 190, g: 231, b: 226)
                cell.statusBtn.backgroundColor = Colors.colorWithRGB(r: 190, g: 231, b: 226)
            }
            
            cell.modeBtn.tag =  10 * indexPath.row + 1
            cell.modeBtn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)

            cell.modeBtn.setTitle(dataOfPlanning["Model"] as? String, for: .normal)
            cell.fromChesisBtn.tag = 10 * indexPath.row + 2
            cell.fromChesisBtn.setTitle(dataOfPlanning["FromChassis"] as? String, for: .normal)
             cell.fromChesisBtn.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
            
            cell.toChessis.tag =  10 * indexPath.row + 3
            cell.toChessis.setTitle(dataOfPlanning["ToChassis"] as? String, for: .normal)
            cell.toChessis.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)

            //cell.toChessis.text = dataOfPlanning.toChassis
            
            cell.qtyBtn.tag =  10 * indexPath.row + 4
            cell.qtyBtn.setTitle(dataOfPlanning["Quantity"] as? String, for: .normal)
            cell.qtyBtn.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)

            
            //cell.qtyTxt.text = dataOfPlanning.quantity
            cell.statusBtn.tag = 10 * indexPath.row + 5
            cell.statusBtn.setTitle(dataOfPlanning["Status"] as? String, for: .normal)
            cell.statusBtn.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
            
            //cell.qtyTxt.text = dataOfPlanning.quantity
            cell.deleteBtn.tag = 10 * indexPath.row + 6
            //cell.deleteBtn.setTitle(dataOfPlanning["Status"] as? String, for: .normal)
            cell.deleteBtn.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
            

            return cell
        }
        else if (indexPath.section == 2) {
           let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.absentiesmCellIdentifier, for: indexPath) as! AbsentiesnTableViewCell
            if (checkEdit == "false"){
                if (sectionPlanningData.count > 0){
                    let dataOfPlanning = otherSectionModels[indexPath.row]
                    cell.txtTotalManPower.text = dataOfPlanning.totalManpower
                    cell.txtPresent.text = dataOfPlanning.present
                    cell.textAbsent.text = dataOfPlanning.absent
                    
                }
            }
            
            cell.txtTotalManPower.tag = 20 * indexPath.row + 1
            cell.txtPresent.tag = 20 * indexPath.row + 2
             cell.textAbsent.tag = 20 * indexPath.row + 3
            return cell
        }
        else if (indexPath.section == 3) {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.safetyCellIdentifier, for: indexPath) as! SafetyTableViewCell
          
            if (checkEdit == "false"){
                if (sectionPlanningData.count > 0)
                {
                    let dataOfPlanning = otherSectionModels[indexPath.row]
                    cell.txtReportable.text = dataOfPlanning.reportable
                    cell.txtNonReportable.text = dataOfPlanning.nonReportable
                    cell.txtFirstAid.text = dataOfPlanning.firstAidInjury
                    cell.txtNearMis.text = dataOfPlanning.nearMiss
                    cell.txtSuggestions.text = dataOfPlanning.suggestions
                }
            }
            
            cell.txtReportable.tag = 30 * indexPath.row + 1
            cell.txtNonReportable.tag = 30 * indexPath.row + 2
            cell.txtFirstAid.tag = 30 * indexPath.row + 3
            cell.txtNearMis.tag = 30 * indexPath.row + 4
            cell.txtSuggestions.tag = 30 * indexPath.row + 5

            
            return cell
        }
        else  {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.assenmbleyIssues, for: indexPath) as! AssembleyIssuesTableViewCell
            if (checkEdit == "true"){
                
            }
            else {
                checkEdit = "true"
                if (sectionPlanningData.count > 0){
                    let dataOfPlanning = otherSectionModels[indexPath.row]
                    cell.txtViewProduction.text = dataOfPlanning.productionIssues
                    cell.txtViewSAfetyIsues.text = dataOfPlanning.safetyIssues
                    cell.txtViewQuality.text = dataOfPlanning.qualityIssues
                }
            }
           
            cell.txtViewProduction.tag = 40 * indexPath.row + 1
            cell.txtViewSAfetyIsues.tag = 40 * indexPath.row + 2
            cell.txtViewQuality.tag = 40 * indexPath.row + 3


            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if (section == 0){
            
           // let addButton = UIButton()
            headerView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60)
            addBtn.center = headerView.center
            //addBtn.frame = CGRect(x: headerView.frame.size.width - 50, y: 0, width: 35, height: 30)
           // addBtn.titleLabel?.textAlignment = .center
            titleLbl.text = "Planning"
            headerView.addSubview(addBtn)
            return headerView
        }
        else if (section == 1){
            let rowView = UIView()
            return rowView
        }
        else if (section == 2){
            let AbsentiesmView = UIView()
            let AbsentiesmLabel = UILabel()
            AbsentiesmLabel.font = UIFont.boldSystemFont(ofSize: 19)
            AbsentiesmLabel.frame = CGRect(x: 10, y: 0, width: 200, height: 50)
            AbsentiesmView.backgroundColor = UIColor(red: 195/255.0, green: 231/255.0, blue: 231/255.0, alpha: 1.0)
            AbsentiesmView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60)
            AbsentiesmLabel.text = "Absentiesm"
            AbsentiesmView.addSubview(AbsentiesmLabel)
            return AbsentiesmView
        }
            
        else if (section == 3){
             let SafetyView = UIView()
            let SafetyLabel = UILabel()
            SafetyLabel.font = UIFont.boldSystemFont(ofSize: 19)
            SafetyLabel.frame = CGRect(x: 10, y: 0, width: 200, height: 50)
            SafetyView.backgroundColor = UIColor(red: 195/255.0, green: 231/255.0, blue: 231/255.0, alpha: 1.0)
            SafetyView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60)
            SafetyLabel.text = "Safety"
            SafetyView.addSubview(SafetyLabel)
            return SafetyView
        }
        else {
            let ASSEMBLYView = UIView()
            let ASSEMBLYLabel = UILabel()
            ASSEMBLYLabel.font = UIFont.boldSystemFont(ofSize: 19)
            ASSEMBLYLabel.frame = CGRect(x: 10, y: 0, width: 320, height: 50)
            ASSEMBLYView.backgroundColor = UIColor(red: 195/255.0, green: 231/255.0, blue: 231/255.0, alpha: 1.0)
            ASSEMBLYView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60)
//            titleLbl.text = "ASSEMBLY ISSUES (PCQDCM)"
            ASSEMBLYLabel.text = "ASSEMBLY ISSUES (PCQDCM)"
            ASSEMBLYView.addSubview(ASSEMBLYLabel)
            return ASSEMBLYView
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 1){
          return 5
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
   
    
    
}
