//
//  SafetyCell.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/6/19.
//

import UIKit

class SafetyCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var suggestionTxt: UITextField!
    
    @IBOutlet weak var nearMiss: UITextField!
    
    @IBOutlet weak var firstAudTxt: UITextField!
    @IBOutlet weak var nonReportableTxt: UITextField!
    @IBOutlet weak var reportableTxt: UITextField!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
