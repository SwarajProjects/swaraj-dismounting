//
//  ViewAssembleyCell.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/6/19.
//

import UIKit

class ViewAssembleyCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var productionTxt: UITextView!
    @IBOutlet weak var qualityTxt: UITextView!
    
    @IBOutlet weak var safetyTxt: UITextView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
