//
//  AssembleyIssuesTableViewCell.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/2/19.
//

import UIKit

class AssembleyIssuesTableViewCell: UITableViewCell {

    @IBOutlet weak var txtViewSAfetyIsues: UITextView!
    @IBOutlet weak var txtViewQuality: UITextView!
    @IBOutlet weak var txtViewProduction: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
