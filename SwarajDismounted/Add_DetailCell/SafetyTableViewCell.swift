//
//  SafetyTableViewCell.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/2/19.
//

import UIKit

class SafetyTableViewCell: UITableViewCell {

    @IBOutlet weak var txtSuggestions: UITextField!
    @IBOutlet weak var txtNearMis: UITextField!
    @IBOutlet weak var txtFirstAid: UITextField!
    @IBOutlet weak var txtNonReportable: UITextField!
    @IBOutlet weak var txtReportable: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
