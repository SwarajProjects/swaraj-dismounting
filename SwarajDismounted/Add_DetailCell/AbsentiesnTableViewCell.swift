//
//  AbsentiesnTableViewCell.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/2/19.
//

import UIKit

class AbsentiesnTableViewCell: UITableViewCell {

    @IBOutlet weak var textAbsent: UITextField!
    @IBOutlet weak var txtPresent: UITextField!
    @IBOutlet weak var txtTotalManPower: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
