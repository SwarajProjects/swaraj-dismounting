//
//  PlanningTableViewCell.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/2/19.
//

import UIKit

class PlanningTableViewCell: UITableViewCell {

    @IBOutlet weak var statusBtn: UIButton!
    @IBOutlet weak var qtyBtn: UIButton!
    @IBOutlet weak var toChessis: UIButton!
    @IBOutlet weak var fromChesisBtn: UIButton!
    @IBOutlet weak var modeBtn: UIButton!
    @IBOutlet var deleteBtn: UIButton!
    
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
