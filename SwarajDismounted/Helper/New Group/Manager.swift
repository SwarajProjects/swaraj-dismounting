//
//  Manager.swift
//  MahindraFame
//
//  Created by Jaspreet Kaur on 2/21/19.
//  Copyright © 2019 Jaspreet Kaur. All rights reserved.
//

import UIKit
import CoreLocation

class Manager: NSObject {
    
    static let instance = Manager()
    
    var gl:CAGradientLayer!
    
    func validateEmailField(_ txtField: UITextField)-> Bool {
        
        if txtField.text?.count == 0 {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: txtField.text)
        
        if result {
            return true
        }
        else {
            return false
        }
        
    }
    
}

// MARK: - Structures
struct MainScreen {
    let size = UIScreen.main.bounds.size
}

struct Colors {
    
    static func colorWithRGB(r: CGFloat, g: CGFloat, b: CGFloat, alpha:CGFloat? = 1.0) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: alpha!)
    }
    
}

extension UITextField {
    
    func attributedPlaceholder(placeholderString: String, placeholderColor: UIColor) -> NSAttributedString {
        return NSAttributedString(string: placeholderString, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
    }
    
    func leftView(withImage:String, tintColor: UIColor) {
        
        self.leftViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 25))
        let image = UIImage(named: withImage)
        imageView.image = image
        imageView.image = imageView.image!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor =  tintColor
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .center
        self.leftView = imageView
    }
    
    func rightView(withImage:String, tintColor: UIColor) {
        
        self.rightViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
        let image = UIImage(named: withImage)
        imageView.image = image
        imageView.image = imageView.image!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor =  tintColor
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .center
        self.rightView = imageView
        
    }
    
    func bottomLine(lineColor: UIColor) {
        
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x:10.0, y: self.frame.size.height - 1, width: UIScreen.main.bounds.width - 80, height: 1.0);
        bottomBorder.backgroundColor = lineColor.cgColor
        self.layer.addSublayer(bottomBorder)
        
    }
    
}

extension UITextView {
    
    func bottomLine() {
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x:0.0, y: self.frame.size.height - 1, width: UIScreen.main.bounds.width - 40, height: 1.0);
        bottomBorder.backgroundColor = UIColor.white.cgColor
        self.layer.addSublayer(bottomBorder)
    }
    
}

extension String {
    
    func stringToString() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let dt = dateFormatter.date(from: self as String)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        return dateFormatter.string(from: dt ?? Date())
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    var stringByRemovingWhitespaces: String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
    
    func sizeWithMyFont(font:UIFont, size:CGSize) -> CGSize{
        let textRext = (self as NSString).boundingRect(with: CGSize(width: size.width, height: size.height), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return CGSize.init(width: CGFloat(ceilf(Float(textRext.width))), height: CGFloat(ceilf(Float(textRext.height))))
    }
    
    static func trim(_ string: String?) -> String {
        return string?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? ""
    }
    
    func stringToDate() -> Date {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let str = formatter.date(from: self as String)
        return str!
    }
    
}

extension UIView {
    
    func lock(tintColor: UIColor = .darkGray) {
        
        DispatchQueue.main.async {
            if let _ = self.viewWithTag(2001) {
                //View is already locked
            }
            else {
                let lockView = UIView(frame: self.bounds)
                lockView.backgroundColor = UIColor(white: 0, alpha: 0.5)
                
                lockView.alpha = 0.5
                let imageView = UIImageView()
                let logoImageView = UIImageView()
                
                if lockView.tag == 4001 {
                    imageView.frame.size.height = 80
                    imageView.frame.size.width = 80
                    logoImageView.frame.size.height = imageView.frame.width/2
                    logoImageView.frame.size.width = imageView.frame.width/2
                } else {
                    imageView.frame.size.height = 80
                    imageView.frame.size.width = 80
                    logoImageView.frame.size.height = imageView.frame.width/2
                    logoImageView.frame.size.width = imageView.frame.width/2
                }
                
                imageView.center = lockView.center
                imageView.image = UIImage(named: "spinner")
                imageView.tintColor = tintColor
                lockView.addSubview(imageView)
                
                lockView.tag = 2001
                logoImageView.frame.size.height = 30
                logoImageView.frame.size.width = 30
                logoImageView.contentMode = .scaleAspectFit
                logoImageView.center = lockView.center
                logoImageView.image = UIImage(named: "")
                lockView.addSubview(logoImageView)
                
                self.rotateView(view: imageView)
                
                let activity = UIActivityIndicatorView(style: .white)
                activity.hidesWhenStopped = true
                activity.center = lockView.center
                //      lockView.addSubview(activity)
                activity.startAnimating()
                self.addSubview(lockView)
                
                UIView.animate(withDuration: 0.2) {
                    lockView.alpha = 1.0
                }
            }
        }
        
    }
    
    func rotateView(view: UIView, duration: Double = 1) {
        
        let kRotationAnimationKey = "com.myapplication.rotationanimationkey"
        if view.layer.animation(forKey: kRotationAnimationKey) == nil {
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
            
            rotationAnimation.fromValue = 0.0
            rotationAnimation.toValue = Float(.pi * 2.0)
            rotationAnimation.duration = duration
            rotationAnimation.repeatCount = Float.infinity
            
            view.layer.add(rotationAnimation, forKey: kRotationAnimationKey)
        }
        
    }
    
    func unlock() {
        
        DispatchQueue.main.async {
            if let lockView = self.viewWithTag(2001) {
                UIView.animate(withDuration: 0.2, animations: {
                    lockView.alpha = 0.0
                }) { finished in
                    lockView.removeFromSuperview()
                }
            }
        }
        
    }
    
    func select(){
        
        let view = UIView(frame: self.bounds)
        view.backgroundColor = UIColor(white: 1.0, alpha: 0.5)
        view.tag = 2002
        
        let tickImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        tickImage.center = view.center
        tickImage.image = UIImage(named: "tick")
        view.addSubview(tickImage)
        
        self.addSubview(view)
        
    }
    
    func unSelect() {
        
        DispatchQueue.main.async {
            if let lockView = self.viewWithTag(2002) {
                UIView.animate(withDuration: 0.2, animations: {
                    lockView.alpha = 0.0
                }) { finished in
                    lockView.removeFromSuperview()
                }
            }
        }
        
    }
    
}

extension Date {
    
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }
    
    func isGreaterThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isGreater = false
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending || self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isGreater = true
        }
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isLess = false
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending  || self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isLess = true
        }
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isEqualTo = false
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        //Return Result
        return isEqualTo
    }
    
    func dateToString() -> String {

        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/YYYY"
        let str = formatter.string(from: self as Date)
        return str
    }
    
    func UTCtoUTCDateOnly() -> Date? {
        //removing time
        let localDateStr = (self as Date).dateString2
        let timezone = NSTimeZone(name: "UTC")
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.timeZone = timezone as TimeZone!
        let UTCDate = formatter.date(from: localDateStr)
        return UTCDate
    }
    
    var dateString2: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.string(from: self as Date)
    }
    
}

extension Manager {
    
    func calculateImageHeight(tableView: UITableView, actualImage: UIImage) -> CGFloat {
        
        let tableViewWidth = tableView.frame.size.width
        let imageHeight = actualImage.size.height
        let imageWidth = actualImage.size.width
        
        let calculatedHeight = ((( tableViewWidth * imageHeight )) / imageWidth) + 10
        
        return calculatedHeight
    }
    
    func getImageUrlFromDocumentDirectory(imageString : String) -> URL{
        let fileManager = FileManager.default
        let dirPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        return dirPath[0].appendingPathComponent(imageString)
    }
    
    func gradientColor(gradientView: UIView, topColor: CGColor, bottomColor: CGColor) {
        self.gl = CAGradientLayer()
        self.gl.frame = gradientView.bounds
        self.gl.colors = [topColor, bottomColor]
        self.gl.locations = [0.0, 1.0]
    }
    
    func customizeString(strings:[String], fontSize:[CGFloat], color:[UIColor], fonts:[String], alignment:NSTextAlignment) -> NSMutableAttributedString {
        
        //         Get All Strings
        
        let attrString = NSMutableAttributedString()
        
        for i in 0..<strings.count{
            let substring: NSString = strings[i] as NSString
            
            // Set Paragraph style for text.
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = alignment
            
            let color = color[i]
            let font:UIFont? = UIFont(name: fonts[i] as String, size: fontSize[i] as CGFloat)
            
            // Make attributed string by adding custom font, custom color and paragraph style in it.
            let attrString1 = NSMutableAttributedString(string: substring as String)
            attrString1.addAttribute(NSAttributedString.Key.font, value: font!, range: NSMakeRange(0, attrString1.length))
            attrString1.addAttribute(NSAttributedString.Key.foregroundColor, value: color ,range: NSMakeRange(0, attrString1.length))
            attrString1.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle ,range: NSMakeRange(0, attrString1.length))
            attrString.append(attrString1)
        }
        return attrString
    }
    
    func showAlert(VC: UIViewController, title: String, message: String, actionButtons : Array<String>, completionHandler: @escaping (_ response: String) -> Void) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for buttonTitle in actionButtons {
            let action = UIAlertAction(title: buttonTitle , style: .default, handler: { (action:UIAlertAction!) in
                completionHandler(action.title!)
            })
            alert.addAction(action)
        }
        VC.present(alert, animated: true, completion: nil)
    }
    
}
