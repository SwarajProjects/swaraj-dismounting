//
//  CalendarViewController.swift
//  MeDiary
//
//  Created by Jaspreet Kaur on 8/23/18.
//  Copyright © 2018 Jaspreet Kaur. All rights reserved.
//

import UIKit
import JTAppleCalendar

protocol GetDate {
    
    func getDateFromCalender(date: String,  machineId: String)
}

class CalendarViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var lblNavigationView: UIView!
    
    @IBOutlet var lblMonth: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet var previousNextView: UIView!
    @IBOutlet weak var lblWeekDay: UILabel!
    @IBOutlet weak var lblMonthYear: UILabel!
    @IBOutlet weak var jtAppleCalendarView: JTAppleCalendarView!
    
    var selectedDateDelegate: GetDate?
    var isFromDate = true
    var machineId: String?
    var isFilterView = Bool()
    
    let formatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Calendar.current.locale
       // dateFormatter.dateFormat = "dd MMM, YYYY"
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter
    }()
    
    let todaysDate = Date()
    
    //MARK: - View life cycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetUp()
    }

    func initialSetUp() {
        
        dateViewSetUp()
        
        DispatchQueue.main.async {
            self.jtAppleCalendarView.reloadData()
        }
        jtAppleCalendarView.scrollDirection = .vertical
        jtAppleCalendarView.minimumInteritemSpacing = 1
        jtAppleCalendarView.minimumLineSpacing = 1
        
        jtAppleCalendarView.scrollToDate(Date(), animateScroll: false)
        jtAppleCalendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            self.setUpCalendarView(from: visibleDates)
        }
    }
    
    func dateViewSetUp() {
        
        let currentDate = Date()
        let currentDateString = formatter.string(from: currentDate)
        if let range = currentDateString.range(of: " ") {
            let monthYear = currentDateString[range.upperBound...]
            lblMonthYear.text = String(monthYear)
        }
        lblWeekDay.text = currentDate.dayOfWeek()
        lblDate.text = currentDateString.components(separatedBy: " ").first
    }
    
    func setUpCalendarView(from dateSegment: DateSegmentInfo) {
        
        guard let date = dateSegment.monthDates.first?.date else {return}
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        let dateString = dateFormatter.string(from: date)
        let subStrings = dateString.components(separatedBy: " ")

        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, animations: {
                self.lblMonth.text = subStrings.first
                self.lblYear.text = subStrings.last
            })
        }
    }
    
    func configureCell(cell: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = cell as? CalendarCollectionViewCell  else { return }
//        handleCellSelection(cell: myCustomCell, cellState: cellState)
        handleCellTextColor(cell: myCustomCell, cellState: cellState)
        handleCellVisibility(cell: myCustomCell, cellState: cellState)
    }
    
    func handleCellSelection(cell: CalendarCollectionViewCell, cellState: CellState) {
        
        if cellState.isSelected {
            cell.backgroundColor = .clear
            cell.lblDate.backgroundColor = UIColor(red: 0/255, green: 154/255, blue: 157/255, alpha: 0.5)
        } else {
            cell.backgroundColor = .clear
            cell.lblDate.backgroundColor = .clear
        }
    }
    
    func handleCellVisibility(cell: CalendarCollectionViewCell, cellState: CellState) {
        cell.isHidden = cellState.dateBelongsTo == .thisMonth ? false : true
    }
    
    func handleCellTextColor(cell: CalendarCollectionViewCell, cellState: CellState) {
        
        let todaysDateString = formatter.string(from: todaysDate)
        let monthDateString = formatter.string(from: cellState.date)
        
        if todaysDateString == monthDateString {
            cell.lblDate.textColor = Constant.appThemeColor
           cell.lblDate.backgroundColor = UIColor(red: 0/255, green: 154/255, blue: 157/255, alpha: 0.3)
            
        } else if cellState.date.isGreaterThanDate(dateToCompare: Date()){
            cell.lblDate.textColor = UIColor.black
            cell.lblDate.backgroundColor = .clear
            
        } else if cellState.date.isLessThanDate(dateToCompare: todaysDate) {
            cell.lblDate.textColor = UIColor.black
            cell.lblDate.backgroundColor = .clear
        }
        
    }
    
    //MARK: - UIButton Actions
    @IBAction func btnTodaysAction(_ sender: Any) {
        jtAppleCalendarView.scrollToDate(Date(), animateScroll: false)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPreviousAction(_ sender: Any) {
        jtAppleCalendarView.scrollToSegment(.previous)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        jtAppleCalendarView.scrollToSegment(.next)
    }
    
}

extension CalendarViewController: JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    
    func calendar(_ calendar: JTAppleCalendarView, shouldSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
        
        
       
        
        if (Constant.editData == "AddScreen"){
            
            
            if (date.UTCtoUTCDateOnly()?.isGreaterThanDate(dateToCompare: Date().UTCtoUTCDateOnly()!))! {
                return true
            } else {
                return false
                
            }
        }
        else {
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "MM/dd/yyyy"
            let showDate = inputFormatter.date(from: date.dateToString())
            inputFormatter.dateFormat = "dd/MM/yyyy"
            let selectedString = inputFormatter.string(from: showDate!)
            print(selectedString)
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            let currentString = formatter.string(from: date)
            if (selectedString == currentString ){
                print(selectedString)
                return true
            }
            else {
               return true
            }
            //return false
        }
        return false
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        let startDate = formatter.date(from: "01 Jan, 2010")
        let endDate = formatter.date(from: "01 Jan, 2100")

        let parameters = ConfigurationParameters(startDate: startDate!,
                                                 endDate: endDate!,
                                                 firstDayOfWeek: .sunday)
        return parameters
    }
    
    func dayDifference(from interval : TimeInterval) -> String
    {
        let calendar = Calendar.current
        let date = Date(timeIntervalSince1970: interval)
        if calendar.isDateInYesterday(date) { return "Yesterday" }
        else if calendar.isDateInToday(date) { return "Today" }
        else if calendar.isDateInTomorrow(date) { return "Tomorrow" }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 1 { return "\(-day) days ago" }
            else { return "In \(day) days" }
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(cell: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: Constant.CellIdentifiers.calendarCell, for: indexPath) as? CalendarCollectionViewCell
        
        configureCell(cell: cell, cellState: cellState)
        
        cell?.lblDate.layer.cornerRadius = (cell?.lblDate.frame.width)! / 2
        cell?.lblDate.clipsToBounds = true
        cell?.lblDate.text = cellState.text
        
        configureCell(cell: cell, cellState: cellState)
        
        return cell ?? CalendarCollectionViewCell()
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setUpCalendarView(from: visibleDates)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "MM/dd/yyyy"
        let showDate = inputFormatter.date(from: date.dateToString())
        inputFormatter.dateFormat = "dd/MM/yyyy"
        let resultString = inputFormatter.string(from: showDate!)
        print(resultString)
        
        configureCell(cell: cell, cellState: cellState)
        
        selectedDateDelegate?.getDateFromCalender(date: resultString, machineId: machineId ?? "")
        dismiss(animated: true)

    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(cell: cell, cellState: cellState)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        jtAppleCalendarView.viewWillTransition(to: size, with: coordinator, anchorDate: Date())
    }
    
}

extension UIView {
    
    func bounce() {
        
        self.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.1, options: UIView.AnimationOptions.beginFromCurrentState, animations: {
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
        
    }
    
}
