//
//  Constant.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/1/19.
//

import Foundation
import UIKit

var isInternetActive : Bool!

public struct Constant{
    
    static var modelsArray = [[String: Any]]()
    static var shiftsArray = [[String: Any]]()
    static var fenderArray = [[String: Any]]()
    static var DeviceToken: String?
    static var DeviceId: String?
    static var userData: User?
    static var listNameArray = [String]()
    static var modelsNameArray = [String]()
    static var editData = "EditScreen"
    static let appThemeColor = UIColor(red: 20/255, green: 179/255, blue: 160/255, alpha: 1.0)
    static let appName = "SwarajDismounted"
    
    static let baseURL = "http://104.211.91.26/dismounting/" as String // Development
    
    struct WebServiceURL {
        static let getUserDetailsForLogin = "api/account/login"
        static var getComponentsByTypeId = "api/Master/GetAllComponentsByTypeId"
        static let getAllModels = "api/master/getallmodels"
        static var getAllShifts = "api/master/getallshifts"
        static let getSearchDetail = "api/Detail/SearchDetail"
        static let getSaveDetails = "api/Detail/SaveDetails"
        static let searchModelDetails = "api/ModelDetail/SearchModel"
        static let submitComplaint = "user/SubmitComplaint"
        static let saveModelDetail = "api/ModelDetail/SaveModelDetails"
        static let saveFenderDetail = "api/ModelDetail/SaveFenderDetails"
        
    }
    
    struct Color {
        static let textFieldPlaceholderColor = UIColor(red: 77/255, green: 77/255, blue: 77/255, alpha: 1.0)
    }
    
    struct UserDefaults {
        static let userDetailDict = "UserDetailsDict"
    }
    
    struct CellIdentifiers {
        static let headerViewTableViewCell = "HeaderViewTableViewCell"
        static let planningCellIdentifier = "planningCellIdentifier"
        static let absentiesmCellIdentifier = "abCellIdentifier"
        static let safetyCellIdentifier = "safetyCellIdentifier"
        static let assenmbleyIssues = "assenmbleyIssues"
        static let calendarCell = "CalendarCell"
        static let ViewDetailPlanningCell = "ViewDetailPlanningCell"
        static let sectionOneCell = "sectionOneCell"
        static let ViewAbsentiesmCell = "ViewAbsentiesmCell"
        static let SafetyCell = "SafetyCell"
        static let ViewAssembleyCell = "ViewAssembleyCell"
        
    }
    
    struct Storyboards {
        static let main = UIStoryboard(name: "Main", bundle: nil)
    }
    
    struct StoryBoardIDs {
        
        static let loginVC = "LoginViewController"
        static let ViewDetailsViewController = "ViewDetailsViewController"
        static let addNewVC = "AddNewViewController"
        static let galleryVC = "GalleryViewController"
        static let suggestionsAndComplaints = "SuggestionsComplaintsViewController"
        static let imagesVC = "ImagesViewController"
        static let videosVC = "VideosViewController"
        static let audiosVC = "AudiosViewController"
        static let aboutKnowledgeCentre = "AboutKnowledgeCentreViewController"
        static let calendarVC = "CalendarViewController"
        static let editVC = "EditViewController"
    }
    
}

struct AlertButton {
    static let Ok = "Ok"
    static let Cancel = "Cancel"
    static let Yes = "Yes"
    static let No = "No"
    static let Gallery = "Gallery"
    static let Camera = "Camera"
}
struct staticText {
    
   
}

