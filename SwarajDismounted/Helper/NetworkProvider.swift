//
//  NetworkProvider.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/2/19.
//

import UIKit
import Alamofire

class NetworkProvider: NSObject {
    
    typealias completionBlock = ([String: Any]) -> Void
    typealias failureBlock = (Error) -> Void
    
    static let shared = NetworkProvider()
    
    func request(_ urlString : String, requestDict : [String :AnyObject]?, method: HTTPMethod, headers: [String: String]?, compBlock : @escaping completionBlock,  failure : @escaping failureBlock) -> Void {
        
        let queue = DispatchQueue.global()
        queue.async {
            //            URLEncoding.default
            Alamofire.request(urlString, method: method, parameters: requestDict, encoding: JSONEncoding.default, headers: headers).responseData(completionHandler: { response in
                var jsonResult:Any!
                
                do {
                    jsonResult = try JSONSerialization.jsonObject(with: response.data!, options: [])
                    
                    var responseDict = [String: Any]()
                    
                    if let jsonDict = jsonResult as? [String: Any] {
                        responseDict = jsonDict
                    }
                    
                    compBlock(responseDict)
                    
                }
                catch {
                    // failure
                    print(error.localizedDescription)
                    failure(error)
                }
            })
        }
    }
    
}
