//
//  StringConstant.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/1/19.
//

import UIKit

class StringConstant: NSObject {

    static let noInternet = "No Internet Connection"
    static let okTitle = "Ok"
    static let cancelTitle = "Cancel"
    static let cancelAlert = "Are you sure you want to Cancel?"
    
    struct SignIn {
        
        static let emptyTokenNumber = "Please enter Username"
        static let invalidEmailId = "Please enter valid Email Id"
        static let emptyPassword = "Please enter Password"
        static let signedIn = "Login Successfully"
    }
    
    struct Details {
        
        static let emptyShift = "Please select Shift"
        static let emptyDate = "Please select Date"
        static let emptyModel = "Please select Model Type"
        static let emptyFender = "Please select Fender Type"
        
    }
    
    struct Logout {
        static let logoutAlert = "Are you sure you want to logout?"
        static let loggedOut = "Logout Successfully"
    }
}
