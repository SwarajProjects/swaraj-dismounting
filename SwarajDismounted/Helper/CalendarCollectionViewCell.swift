//
//  CalendarCollectionViewCell.swift
//  SwarajFTR
//
//  Created by Jaspreet Kaur on 05/09/18.
//  Copyright © 2018 Jaspreet Kaur. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarCollectionViewCell: JTAppleCell {
    
     @IBOutlet weak var lblDate: UILabel!
    
}
