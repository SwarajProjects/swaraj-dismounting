//
//  ViewDetailViewModel.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/2/19.
//

import UIKit

class ViewDetailViewModel: NSObject {
    
     var modelsArray = [[String: Any]]()
   static func getAllShifts(completionHandler: @escaping (_ success: [[String: Any]], _ error: Error?) -> Void) {
        
        NetworkProvider.shared.request(Constant.baseURL + Constant.WebServiceURL.getAllShifts, requestDict: nil, method: .get, headers: nil, compBlock: { (responseDict) in
            
            let responseCode = responseDict["StatusCode"] as? Int ?? 0
            
            if responseCode == 404 {
               
            } else if responseCode ==  200 {
                
                //                self.view.unlock()
                let modelsArray = responseDict["ResultData"] as? [[String: Any]] ?? []
                completionHandler(modelsArray, nil)
                
            } else if responseCode == 400 {
                
                //                self.view.unlock()
                let message = responseDict["Message"] as? String
                //                self.view.makeToast(message)
                
            }
        }) { (error) in
            print(error.localizedDescription)
            //            self.view.unlock()
        }
        
    }
    
    static func onClicKSearchApi(params: [String: AnyObject], completionHandler: @escaping (_ success: [String: AnyObject],_ message : String, _ error: Error?) -> Void) {
        
      
        NetworkProvider.shared.request(Constant.baseURL + Constant.WebServiceURL.getSearchDetail, requestDict: params, method: .post, headers: nil, compBlock: { (responseDict) in
            
            let responseCode = responseDict["StatusCode"] as? Int ?? 0
            
            if responseCode == 404 {
                completionHandler([:], responseDict["Message"] as? String ?? "", nil)
                
            } else if responseCode ==  200 {
                
                //                self.view.unlock()
                let modelsArray = responseDict["ResultData"] as? [String: AnyObject] ?? [:]
                completionHandler(modelsArray, responseDict["Message"] as? String ?? "", nil)
                
            } else if responseCode == 400 {
                
                //                self.view.unlock()
                let message = responseDict["Message"] as? String
                completionHandler([:], responseDict["Message"] as? String ?? "", nil)
                
            }
            else {
                completionHandler([:], responseDict["Message"] as? String ?? "", nil)
            }
        }) { (error) in
            print(error.localizedDescription)
            //            self.view.unlock()
        }
    }

}
