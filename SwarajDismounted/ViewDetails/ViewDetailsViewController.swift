//
//  ViewDetailsViewController.swift
//  SwarajDismounted
//
//  Created by Gurwinder Singh on 5/2/19.
//

import UIKit
import DropDown
class ViewDetailsViewController: UIViewController {
    
     // MARK: - View all Constants and Variables
    var listNameArray = [String]()
    var user : User?
    var planningModel : [LstPlanning]!
    var otherSectionModels :[Json4Swift_Base]!
    var shiftId = 1
    var currentTxt = UITextField()
    var currentBtn = UIButton()
    var viewPlanningData = [[String:AnyObject]]()
    var viewPlan = [[String: AnyObject]]()
    var shiftArray = [String]()
    let backLbel = UILabel ()
    
    // MARK: - Outlets
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtShift: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var viewHeaderView: UIView!
    @IBOutlet weak var viewTitleLbl: UILabel!
    @IBOutlet weak var test: UIButton!
    @IBOutlet weak var txtProduction: UITextField!
    @IBOutlet weak var editWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var buttonEditOutlet: UIButton!
    @IBOutlet weak var buttonAddNewOutlet: UIButton!
    @IBOutlet weak var btnEditWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnAddNewWidthConstraint: NSLayoutConstraint!
    
    // MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        editWidthConstant.constant = 0
        backLbel.text = "No Record Found"
        backLbel.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: self.tableView.frame.height)
        backLbel.textAlignment = .center
        
        setShiftAndModel()
        setModels()
        
        if Constant.userData?.isEditRights == 0 {
            btnAddNewWidthConstraint.constant = 0
            editWidthConstant.constant = 0
        } else {
            btnAddNewWidthConstraint.constant = 100
            editWidthConstant.constant = 100
        }
        
          //  setShiftApi()
    }
    
    // MARK: - View all button actions
    func setShiftAndModel(){
      self.view.lock(tintColor: Constant.appThemeColor)
        ViewDetailViewModel.getAllShifts(completionHandler: { (response, error) in
            for component in response {
                let shift = component["Shift"] as! String
               Constant.listNameArray.append(shift)
                self.setUpInitialViews()
                self.view.unlock()
            }
        })
    }
   
    func setModels(){
        AddDetailViewModel.getAllModels(completionHandler: { (response, error) in
            
            if (Constant.modelsNameArray.count <= 0){
                for component in response {
                    
                    let shift = component["Model"] as! String
                    Constant.modelsNameArray.append(shift)
                    
                }
            }
        })
    }
    
    @IBAction func testBtn(_ sender: Any) {
        if isInternetActive {
                setButtonDropDown()
        }
        else {
            self.view.makeToast(StringConstant.noInternet)
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Constant.editData = "EditScreen"
    }
    
    @IBAction func onClickEditBtn(_ sender: Any) {
        
        if (planningModel != nil && otherSectionModels != nil){
            let viewController: AddDetailViewController  = storyboard?.instantiateViewController(withIdentifier: "AddDetailViewController") as! AddDetailViewController
            viewController.planningModel = planningModel
            viewController.otherSectionModels = otherSectionModels
            viewController.isEditBool = "true"
            viewController.checkEdit = "false"
            let x : Int = otherSectionModels[0].detailId ?? 0
            let myString = String(x)
            viewController.detailID = myString
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func onClickLogoutBtn(_ sender: Any) {
        showAlertToLogout()
    }
    
    @IBAction func onclickSearchBtn(_ sender: Any) {
        
        if (txtDate.text == "") {
            view.makeToast("Please fill the date")
            return
        }
        
        view.lock(tintColor: Constant.appThemeColor)
        var param = [String: AnyObject]()
        param["Date"] = txtDate.text as AnyObject

        let shift  = String(describing: self.shiftId)
        param["Shift"] = shift as AnyObject
        if !isInternetActive {
            self.view.makeToast(StringConstant.noInternet)
            return
        }
        
        ViewDetailViewModel.onClicKSearchApi(params: param) { (response,message, error) in
             self.view.unlock()
           
            if (response.count > 0){
                self.otherSectionModels = Json4Swift_Base.modelsFromDictionaryArray(array: [response])
                
                self.txtProduction.text = self.otherSectionModels[0].totalProduction
                self.planningModel =
                    LstPlanning.modelsFromDictionaryArray(array: response["lstPlanning"] as! [[String: Any]])
             
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                let currentString = formatter.string(from: date)
                if (self.txtDate.text == currentString ){
                    self.editWidthConstant.constant = 100
                }
                else {
                    self.editWidthConstant.constant = 0
                }
                self.tableView.reloadData()
            }
            else {
                if (self.planningModel != nil){
                    self.editWidthConstant.constant = 0
                    self.planningModel = nil
                }
                if (self.otherSectionModels != nil){
                    self.editWidthConstant.constant = 0
                    self.otherSectionModels = nil
                }
                self.txtProduction.text = ""
                self.tableView.reloadData()
                self.view.makeToast(message)
            }
        }
    }
    
    @IBAction func onClickAddDetail(_ sender: Any) {
        let viewController: AddDetailViewController  = storyboard?.instantiateViewController(withIdentifier: "AddDetailViewController") as! AddDetailViewController
         viewController.isEditBool = "false"
        viewController.delegate =   self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    // Mark:  // MARK: - View all functions
    
     func setButtonDropDown() {
        
        view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: currentTxt.bounds.height)
        dropDown.anchorView = test
        
        if test == test {
            
            if (Constant.listNameArray.count > 0){
               dropDown.dataSource = Constant.listNameArray
            }
            else {
                setShiftAndModel()
                return
            }
                //dropDown.dataSource = Constant.listNameArray
                dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                    self.test.setTitle(item, for: .normal)
                    // self.currentTxt.text = item
                    self.shiftId = index + 1
                    if self.currentTxt == self.txtShift {
                        
                    }
                    dropDown.hide()
                }
                dropDown.show()
        }
        
    }
    
    func setShiftApi(){
        
        if isInternetActive {
            
            view.lock(tintColor: Constant.appThemeColor)
            ViewDetailViewModel.getAllShifts(completionHandler: { (response, error) in
                for component in response {
                    let shift = component["Shift"] as! String
                    let array = shift.components(separatedBy: " ")
                    if (array.count > 0){
                        self.shiftArray.append( array[1])
                    }
                }
            })
        }
        else {
             view.unlock()
            self.view.makeToast(StringConstant.noInternet)
        }
      
    }
    func  setUpInitialViews(){
        txtProduction.layer.borderWidth = 1
        txtProduction.layer.borderColor = UIColor.black.cgColor
        
        txtDate.rightView(withImage: "calendar", tintColor: .darkGray)
        txtDate.layer.borderWidth = 1
        txtDate.layer.borderColor = UIColor.black.cgColor
        
       // test.layer.cornerRadius = 5
        if (Constant.listNameArray.count > 0){
          test.setTitle(Constant.listNameArray[0] as String, for: .normal)
            self.shiftId = 1
        }
        else {
            test.setTitle("", for: .normal)
            self.shiftId = 0
        }
        test.layer.borderWidth = 1
        test.layer.borderColor = UIColor.black.cgColor
    }
    
    func showAlertToLogout() {
        
        Manager.instance.showAlert(VC: self, title: Constant.appName, message: StringConstant.Logout.logoutAlert, actionButtons: [AlertButton.No, AlertButton.Yes]) { (response) in
            
            if response == AlertButton.Yes {
                
                UserDefaults.standard.removeObject(forKey: Constant.UserDefaults.userDetailDict)
                Constant.userData = nil
                self.view.makeToast(StringConstant.Logout.loggedOut)
                
                if let loginVC = Constant.Storyboards.main.instantiateViewController(withIdentifier: Constant.StoryBoardIDs.loginVC) as? LoginViewController {
                    
                    self.navigationController?.pushViewController(loginVC, animated: false)
                }
                
            }
            
        }
        
    }
}

extension ViewDetailsViewController : ChildViewControllerDelegate {
    
    func childViewControllerResponse(detail: [Json4Swift_Base], plan: [LstPlanning]) {
        
        if (detail != nil && plan != nil) {
            
        }
        otherSectionModels = nil
        //otherSectionModels = detail
         txtDate.text = ""
        if (Constant.listNameArray.count > 0){
             test.setTitle(Constant.listNameArray[0] as String, for: .normal)
            self.shiftId = 1
        }
        else {
            test.setTitle("", for: .normal)
            self.shiftId = 1
        }
        txtProduction.text = ""
        planningModel = nil
        editWidthConstant.constant = 0
        tableView.reloadData()
    }
}
